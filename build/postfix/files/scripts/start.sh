#!/bin/bash

/bin/echo $NEXTCLOUD_DOMAIN_NAME > /etc/mailname
/bin/cp /etc/resolv.conf  /var/spool/postfix/etc/
/etc/init.d/rsyslog start

/usr/sbin/postfix start && /bin/sleep 5 && /usr/bin/tail -f /var/log/mail.*
