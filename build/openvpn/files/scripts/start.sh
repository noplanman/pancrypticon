#!/bin/bash

if [ ! $TESTING ]; then
    while [ ! -e /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem ]; do
        /bin/echo " - Waiting for LE cert to generate..."
        /bin/sleep 5
    done
fi

/bin/cp /tmp/gibberfish.* /opt/openvpn/
/bin/cp /tmp/ca.crt /opt/openvpn/

/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/openvpn/openvpn.conf
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /opt/openvpn/gibberfish.conf /opt/openvpn/gibberfish.ovpn
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/openvpn/nextcloud_auth.py

if [ ! -f /opt/openvpn/vpnclient.key ]; then
    cd /opt/openvpn
    /usr/sbin/openvpn --genkey --secret vpnclient.key
fi

cd /opt/openvpn
/usr/bin/zip -FS vpnclient.zip gibberfish.* vpnclient.key ca.crt

/usr/bin/sec --detach --conf=/etc/sec/letsencrypt.rules \
    --input=/etc/letsencrypt/letsencrypt.log

/sbin/iptables -t nat -A POSTROUTING -s 10.8.0.0/24 ! -d 172.18.0.0/24 -o eth0 \
    -j MASQUERADE && \
/usr/sbin/openvpn --cd /etc/openvpn --config /etc/openvpn/openvpn.conf
