#!/bin/bash

/usr/local/sbin/config.sh && \

# Start loolwsd
/bin/su -c "/usr/bin/nice -n -19 /usr/bin/loolwsd \
    --version \
    --o:sys_template_path=/opt/lool/systemplate \
    --o:lo_template_path=/opt/collaboraoffice5.3 \
    --o:child_root_path=/opt/lool/child-roots \
    --o:file_server_root_path=/usr/share/loolwsd" \
    -s /bin/bash lool &

# For some reason loolwsd doesn't communicate with nextcloud properly the first
# time it's started, so the following is a workaround until they fix the issue
# https://github.com/CollaboraOnline/Docker-CODE/issues/16

/bin/sleep 45
/sbin/killall5 -9 loolwsd

/bin/su -c "/usr/bin/nice -n -19 /usr/bin/loolwsd \
    --version \
    --o:sys_template_path=/opt/lool/systemplate \
    --o:lo_template_path=/opt/collaboraoffice5.3 \
    --o:child_root_path=/opt/lool/child-roots \
    --o:file_server_root_path=/usr/share/loolwsd" \
    -s /bin/bash lool
