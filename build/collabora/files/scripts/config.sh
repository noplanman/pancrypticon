#!/bin/bash

# Fix lool resolv.conf problem (wizdude)
/bin/rm /opt/lool/systemplate/etc/resolv.conf && \
/bin/ln -s /etc/resolv.conf /opt/lool/systemplate/etc/resolv.conf

/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/loolwsd/loolwsd.xml && \
/bin/sed -i \
    "s|\"\"></server_name>|\"\">${server_name}</server_name>|g" \
    /etc/loolwsd/loolwsd.xml
