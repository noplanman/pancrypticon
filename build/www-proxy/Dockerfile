FROM debian:stretch

LABEL maintainer "blacksam@gibberfish.org"

ENV DEBIAN_FRONTEND noninteractive

RUN /usr/bin/apt-get update  > /dev/null && \
    /usr/bin/apt-get  install -y \
    cron \
    curl \
    letsencrypt \
    nginx \
    rng-tools \
    ssl-cert  > /dev/null && \
    /usr/bin/apt-get  clean

COPY files/scripts/* /usr/local/sbin/
COPY files/etc/nginx/nginx-certbot.conf /etc/nginx/
COPY files/etc/nginx/sites-enabled/* /etc/nginx/sites-enabled/
COPY files/etc/nginx/conf.d/* /etc/nginx/conf.d/
COPY files/etc/cron.d/* /etc/cron.d/
COPY files/etc/letsencrypt/* /etc/letsencrypt/
COPY files/var/www/html/* /tmp/

EXPOSE 80
EXPOSE 443

HEALTHCHECK CMD /usr/bin/curl --fail --silent --location http://localhost

CMD /usr/local/sbin/start.sh
