#!/bin/bash

/bin/mkdir -p /var/www/html/custom_50x/
/bin/cp -p /tmp/custom_50x.html /var/www/html/custom_50x/
/bin/cp -p /tmp/gibberfish-logo.png /var/www/html/custom_50x/
/bin/cp -p /tmp/pancrypticon-logo.png /var/www/html/custom_50x/

/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/nginx/sites-enabled/* && \
/bin/sed -i "s|OFFICE_DOMAIN_NAME|$OFFICE_DOMAIN_NAME|g" \
    /etc/nginx/sites-enabled/* && \
/bin/sed -i "s|HIDDEN_DOMAIN_NAME|$HIDDEN_DOMAIN_NAME|g" \
    /etc/nginx/sites-enabled/* && \
/bin/sed -i "s|NEXTCLOUD_THEME|$NEXTCLOUD_THEME|g" \
    /etc/nginx/sites-enabled/* && \
/usr/bin/find /var/www/html -name *.html -exec \
    /bin/sed -i "s|NEXTCLOUD_THEME|$NEXTCLOUD_THEME|g" {} \;

if [ $TESTING ]; then
    /bin/sed -i "s|SSLCertificateChainFile|#SSLCertificateChainFile|g" \
        /etc/nginx/sites-enabled/default-ssl
else
    /bin/sed -i "s|#SSLCertificateChainFile|SSLCertificateChainFile|g" \
        /etc/nginx/sites-enabled/default-ssl
fi
