<?php
$CONFIG = array (
    'datadirectory' => '/opt/nextcloud-data',
    'default_language' => 'LANGUAGE',
    'enable_previews' => true,
    'enabledPreviewProviders' => array(
        'OC\Preview\Image',
    ),
    'filelocking.enabled' => true,
    'htaccess.RewriteBase' => '/',
    'log_rotate_size' => 104857600,
    'mail_domain' => 'NEXTCLOUD_DOMAIN_NAME',
    'mail_from_address' => 'no-reply',
    'mail_smtpauth' => false,
    'mail_smtphost' => 'postfix',
    'mail_smtpmode' => 'smtp',
    'mail_smtpport' => '25',
    'memcache.local' => '\OC\Memcache\Redis',
    'memcache.locking' => '\OC\Memcache\Redis',
    'mysql.utf8mb4' => true,
    'redis' => array(
        'host' => 'redis',
        'port' => 6379,
    ),
    'skeletondirectory' => '/opt/nextcloud-data/skeleton_files',
    'theme' => 'NEXTCLOUD_THEME',
    'token_auth_enforced' => true,
    'trusted_domains' =>
        array (
            0 => 'NEXTCLOUD_DOMAIN_NAME',
            1 => 'HIDDEN_DOMAIN_NAME',
        ),
    'updatechecker' => false,
    'upgrade.disable-web' => true,
);
