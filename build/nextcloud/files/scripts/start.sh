#!/bin/bash

# install/upgrade nextcloud if not already current
/usr/local/sbin/install.sh && \

/etc/init.d/cron start && \

# start nginx
/etc/init.d/php7.0-fpm start && \
/usr/sbin/nginx -g 'daemon off;'
