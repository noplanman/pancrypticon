#!/bin/bash

function version { /bin/echo "$@" | /usr/bin/awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }

# if this is a new installation, or an older version is currently installed...
if [ ! -f /opt/nextcloud-data/.installed ] || [ $(version $NEXTCLOUD_VERSION) \
    -gt $(version $(</opt/nextcloud-data/.installed)) ]; then
    /bin/echo "*** Installing Nextcloud ${NEXTCLOUD_VERSION}..."
    cd /opt && \
    ## download and verify nextcloud tarball
    /usr/bin/wget --quiet https://download.nextcloud.com/server/releases/nextcloud-$NEXTCLOUD_VERSION.tar.bz2 && \
    /usr/bin/wget --quiet https://download.nextcloud.com/server/releases/nextcloud-$NEXTCLOUD_VERSION.tar.bz2.sha256 && \
    /usr/bin/wget --quiet https://download.nextcloud.com/server/releases/nextcloud-$NEXTCLOUD_VERSION.tar.bz2.asc && \
    /usr/bin/wget --quiet https://nextcloud.com/nextcloud.asc && \
    /usr/bin/gpg --import -v nextcloud.asc && \
    /usr/bin/gpg --verify -v nextcloud-$NEXTCLOUD_VERSION.tar.bz2.asc nextcloud-$NEXTCLOUD_VERSION.tar.bz2 && \
    /usr/bin/sha256sum --strict -c nextcloud-$NEXTCLOUD_VERSION.tar.bz2.sha256 < nextcloud-$NEXTCLOUD_VERSION.tar.bz2 && \

    # backup the config
    if [ -f nextcloud/config/config.php ]; then
        /bin/cp -p nextcloud/config/config.php /opt/config.php.bak
    fi

    # remove the old version
    /bin/rm nextcloud/* -rf

    # unpack tarballs
    /bin/tar -xf nextcloud-$NEXTCLOUD_VERSION.tar.bz2 && \
    /usr/bin/touch /opt/nextcloud-data/.ocdata && \

    if [ -f /opt/config.php.bak ]; then
        /bin/mv -f /opt/config.php.bak nextcloud/config/config.php
    fi

    # create the admin's initial files folder
    if [ ! -d /opt/nextcloud-data/admin/files ]; then
      /bin/mkdir -p -m 750 /opt/nextcloud-data/admin/files && \
      /bin/cp -prv /opt/nextcloud-data/skeleton_files/* \
          /opt/nextcloud-data/admin/files
    fi

    # enforce permissions
    /usr/bin/find /opt/nextcloud* -not -user www-data -not -group www-data \
        -exec /bin/chown -R www-data:www-data {} \; && \
    /usr/bin/find /opt/nextcloud* -type d -not -perm 750 \
        -exec /bin/chmod 750 {} \; && \
    /usr/bin/find /opt/nextcloud* -type f -not -perm 640 \
        -exec /bin/chmod 640 {} \;
fi

# copy custom themes and configs
/bin/cp -pr /usr/local/share/themes/* /opt/nextcloud/themes/ && \
/bin/cp /usr/local/etc/pancrypticon.config.php /opt/nextcloud/config/ && \
/bin/mkdir -p /opt/nextcloud-data/news/config && \
/bin/cp /tmp/news_appconfig.ini /opt/nextcloud-data/news/config/config.ini && \
/bin/cp -pr /tmp/newsfeeds /opt/nextcloud-data/ && \
# /bin/cp -pr /tmp/core/templates/filetemplates /opt/nextcloud/core/templates/ && \

# substitute values from settings.env
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /opt/nextcloud/config/pancrypticon.config.php && \
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /opt/nextcloud-data/news/config/config.ini && \
/bin/sed -i "s|NEXTCLOUD_THEME|$NEXTCLOUD_THEME|g" \
    /opt/nextcloud/config/pancrypticon.config.php && \
/bin/sed -i "s|LANGUAGE|$LANGUAGE|g" \
    /opt/nextcloud/config/pancrypticon.config.php && \

# add the hidden domain to the trusted list
if [ ! -z $HIDDEN_DOMAIN_NAME ]; then
    /bin/sed -i "s|HIDDEN_DOMAIN_NAME|$HIDDEN_DOMAIN_NAME|g" \
        /opt/nextcloud/config/pancrypticon.config.php
fi && \

# Do not proceed until the database is available
while [ `/bin/nc -z mariadb 3306` ]; do
    /bin/echo "*** Waiting for database container to initialize..."
    /bin/sleep 3
done && \

cd /opt/nextcloud && \

if [ ! -f /opt/nextcloud-data/.installed ]; then
    # install nextcloud
    /bin/echo "*** Initializing new installation..."
    /usr/bin/sudo -u www-data php occ maintenance:install --database "mysql" \
    --database-name "nextcloud"  --database-user "$MYSQL_USER" \
    --database-host "mariadb" --database-pass "$MYSQL_PASSWORD" --admin-user "admin" \
    --admin-pass "$ADMIN_PASSWORD" --no-interaction && \

    # toggle some initial database parameters
    /bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" /opt/initial_settings.sql && \
    /bin/sed -i "s|OFFICE_DOMAIN_NAME|$OFFICE_DOMAIN_NAME|g" /opt/initial_settings.sql && \
    /bin/sed -i "s|TURN_SECRET|$TURN_SECRET|g" /opt/initial_settings.sql && \
    /bin/sed -i "s|TURN_SERVER|$TURN_SECRET|g" /opt/initial_settings.sql && \
    /bin/sed -i "s|OJSXC_TOKEN|$OJSXC_TOKEN|g" /opt/initial_settings.sql && \
    /bin/sed -i "s|ADMIN_EMAIL|$ADMIN_EMAIL|g" /opt/initial_settings.sql && \
    /bin/echo "*** Configuring initial app settings..." && \
    mysql -h mariadb -u $MYSQL_USER -p"$MYSQL_PASSWORD" nextcloud < /opt/initial_settings.sql
else
    # run any needed updates/migrations
    /bin/echo "*** Running updates/migrations..."
    /usr/bin/sudo -u www-data php /opt/nextcloud/occ upgrade --no-interaction
    /usr/bin/sudo -u www-data php /opt/nextcloud/occ db:add-missing-indices
    # upconvert older schemas to use utf8mb4
    /usr/bin/sudo -u www-data php /opt/nextcloud/occ db:convert-mysql-charset
fi && \

# install additional apps
/bin/echo "*** Installing apps from appstore..."
/usr/local/sbin/install_apps.py

# (en|dis)able built-in apps
/bin/echo "*** Configuring built-in apps..."
/usr/bin/sudo -u www-data php occ app:disable firstrunwizard && \
/usr/bin/sudo -u www-data php occ app:disable sharebymail && \
/usr/bin/sudo -u www-data php occ app:disable survey_client && \
/usr/bin/sudo -u www-data php occ app:disable theming && \
/usr/bin/sudo -u www-data php occ app:disable updatenotification && \
/usr/bin/sudo -u www-data php occ app:enable files_retention && \
/usr/bin/sudo -u www-data php occ app:enable logreader
/usr/bin/sudo -u www-data php occ app:enable encryption && \
/usr/bin/sudo -u www-data php occ encryption:enable && \

# run the updater in case this was an upgrade
#/usr/bin/sudo -u www-data php occ maintenance:update:htaccess && \

# update the installed version file
/bin/echo -n "${NEXTCLOUD_VERSION}" > /opt/nextcloud-data/.installed
/bin/echo "*** Installation/updates complete..."
