## Overview

Pancrypticon brings together docker, nextcloud, collabora online, spreed, and a
host of other apps in one easy-to-use platform that "just works" out of the box.
We want encryption and privacy to be accessible to everyone, regardless of
technical skill or financial resources, so pancrypticon handles the installation,
configuration, and integration of all of the moving parts with a minimum of user
effort.

Pancrypticon is being developed by [Gibberfish, Inc](https://gibberfish.org), an all-volunteer nonprofit
dedicated to providing free, secure, hosted cloud tools to other nonprofits,
organizers, activists, and human rights advocates worldwide. It is also freely
available for use by anyone, from home users to organizations who wish to host
their own on premises solution. If you like this project, please consider [donating](https://www.gibberfish.org/donate/).

## Project status

Pancrypticon is actively maintained, and **we welcome open source community
contributions**, especially in the areas of security hardening and performance.
Please have a look at [our wiki](https://gitlab.com/gibberfish/pancrypticon/wikis/home) for more information.


## Sub-projects

Portions of this software are distributed as pre-built docker images. Wherever
feasible we use official images from the upstream maintainers, but in situations
where we need better control over the content or configuration we maintain our
own custom builds.
