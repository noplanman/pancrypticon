#!/bin/bash

CONFIG_DB="/home/daygate/daygate/db.sqlite3"
UPDATES_ENABLED=`echo "SELECT value FROM deploy_config WHERE key = 'auto_update_pancrypticon';" | \
    sqlite3 $CONFIG_DB`

if [[ $UPDATES_ENABLED == 'False' || $UPDATES_ENABLED == '' ]]; then
    exit 0
fi

cd /opt/pancrypticon/pancrypticon && \

GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`

# check for updates
torsocks git remote update 2>&1 > /dev/null
if [[ `git status --untracked-files=no 2>&1 | \
        grep -c "Your branch is behind"` -eq "1" \
]];
then
    torsocks git pull && sudo scripts/verify-checksums.py || exit $?
    torsocks docker-compose pull
    docker-compose up -d --build --remove-orphans
    # run any needed upgrades on the db/tables
    sleep 30 && \
    docker exec -it mariadb mysql_upgrade -p"${MYSQL_ROOT_PASSWORD}"
fi

# remove old containers, images, etc
docker system prune --all --force
docker rmi $(docker images -f "dangling=true" -q)
